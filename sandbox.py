# Importamos la clase Palabra
from Palabra import Palabra

p = Palabra()

# Elegimos una palabra aleatoria y creamos una palabra vacía
p.elegir_palabra()
p.crear_palabra_vacia()

# Imprimimos la palabra vacía inicial
print("La palabra es:", " ".join(p.palabra_vacia))

# Bucle para verificar letras
while True:
    letra = input("Ingrese una letra (o 'q' para salir): ")
    if letra == "q":
        print("Saliendo del programa...")
        break
    encontrado = p.verificar_letra(letra)
    if encontrado:
        print("Letra encontrada. La palabra ahora es:", " ".join(p.palabra_vacia))
    else:
        print("Letra no encontrada. La palabra sigue siendo:", " ".join(p.palabra_vacia))
pass