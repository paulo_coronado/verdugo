from Palabra import Palabra
from Figura import Figura
from GUI import GUI

class Juego:

    def __init__(self):
        self.cantidadIntentos=10
        self.intentos=0
        self.ganador=False
        self.palabra= Palabra()
        self.figura=Figura()
        self.gui=GUI()
        self.win=0

    def definirIntentos(self):
        self.intentos+=1

    def juego(self):   
        self.palabra.elegir_palabra() 
        self.palabra.crear_palabra_vacia()
    
        seguirJugando=True
        while self.intentos < self.cantidadIntentos or seguirJugando:
            letra=self.gui.pedirLetra()
            if not (self.palabra.verificar_letra(letra)):
                self.intentos=self.intentos+1               
                self.gui.mostrarFigura(self.intentos, self.figura.figura)
                self.gui.mostrarpalabra(self.palabra.palabra_vacia)
                self.win=+1
            else:
                self.gui.mostrarFigura(self.intentos, self.figura.figura)
                self.gui.mostrarpalabra(self.palabra.palabra_vacia)                
 
            if  self.intentos ==9:
                
                print("Has perdido el juego del verdugo")
                break
            
            if self.win == len(self.palabra.palabra_escogida):
                
                print ("Has ganado jiji")
                break

miJuego=Juego()
miJuego.juego()
