import requests
import random

class Palabra:
    def __init__(self):
        self.palabra_escogida = ""
        self.palabra_vacia = []

    def elegir_palabra(self):
        response = requests.get("https://random-word-api.herokuapp.com/word?number=1")
        diccionario = response.json()
        self.palabra_escogida = random.choice(diccionario)
    
    def crear_palabra_vacia(self):
        self.palabra_vacia = ["_"] * len(self.palabra_escogida)
    
    def verificar_letra(self, letra):
        encontrado = False
        for i, l in enumerate(self.palabra_escogida):
            if l == letra:
                encontrado = True
                self.palabra_vacia[i] = letra
        return encontrado

    pass