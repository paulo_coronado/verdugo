class Figura:
    
    def __init__(self):
        self.figura = ['cabeza', 'torso', 'brazoDer', 'brazoIzq', 'piernaDer', 'piernaIzq','suelo', 'cadalso', 'cuerda']
        self.paso=0
        
    def agregarPaso(self):
        self.paso+=1
    
    def borraPaso(self):
        self.paso-=1
        
    def limpiarPasos(self):
        self.paso=0